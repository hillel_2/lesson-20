import logging

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, FormView

from market.forms import WallpaperForm
from market.models import Wallpaper


logger = logging.getLogger('MarketView')


@permission_required(perm='market.view_wallpaper')
def list(request):
    wallpapers = Wallpaper.objects.all()
    return render(request,
                  'market/wallpapers.html',
                  context={'wallpapers': wallpapers, 'injected_text': '<script>alert("FAA")</script>'})


@login_required
def item(request, item_id):
    wallpaper = Wallpaper.objects.get(slug=item_id)  # Compiled to Select * from wallppaper where slug=:item_id
    tags = wallpaper.tags.all()
    return render(request,
                  'market/wallpaper.html',
                  context={'wallpaper': wallpaper, 'tags': tags})


@staff_member_required
def add(request):
    if request.method == 'POST':
        form = WallpaperForm(request.POST, request.FILES)
        if form.is_valid():
            new_wallpaper = form.save(commit=False)
            new_wallpaper.creator = request.user
            new_wallpaper.save()
            form.save_m2m()
            return redirect('market:item', item_id=new_wallpaper.slug)
        return render(request, 'market/new_wallpaper.html', context={'form': form})
    form = WallpaperForm()
    return render(request, 'market/new_wallpaper.html', context={'form': form})


@login_required
def update(request, item_id):
    wallpaper = Wallpaper.objects.get(slug=item_id)
    form = WallpaperForm(instance=wallpaper)
    return render(request,
                  'market/edit_wallpaper.html',
                  context={'form': form, 'slug': item_id})


class ItemView(View):
    def get(self, request, item_id):
        wallpaper = Wallpaper.objects.get(slug=item_id)
        tags = wallpaper.tags.all()
        return render(request,
                      'market/wallpaper.html',
                      context={'wallpaper': wallpaper, 'tags': tags})

    def post(self, request, item_id):
        wallpaper = Wallpaper.objects.get(slug=item_id)
        form = WallpaperForm(request.POST, request.FILES, instance=wallpaper)
        if form.is_valid():
            form.save()
            return redirect('market:wallpaper', item_id=item_id)
        return redirect('market:edit', item_id=item_id)


class WallpapersView(ListView):
    model = Wallpaper
    ordering = '-name'

    def get_ordering(self):
        return self.request.GET.get('orderBy', self.ordering)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['injected_text'] = 'You\'ve been hacked'
        return context


class WallpaperItemView(DetailView):
    model = Wallpaper
    slug_url_kwarg = 'item_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object:
            context['tags'] = self.object.tags.all()
        return context


class WallpaperAddFormView(FormView):
    template_name = 'market/new_wallpaper.html'
    form_class = WallpaperForm

    def form_valid(self, form):
        new_wallpaper = form.save(commit=False)
        new_wallpaper.creator = self.request.user
        new_wallpaper.save()
        form.save_m2m()
        return redirect('market:item', item_id=new_wallpaper.slug)


class WallpaperCreateView(LoginRequiredMixin, CreateView):
    model = Wallpaper
    form_class = WallpaperForm

    def form_valid(self, form):
        new_wallpaper = form.save(commit=False)
        new_wallpaper.creator = self.request.user
        new_wallpaper.save()
        form.save_m2m()
        return super().form_valid(form)
