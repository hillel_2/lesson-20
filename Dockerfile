FROM alpine:3.14

RUN apk add python3-dev py3-pip libjpeg jpeg-dev postgresql-dev zlib-dev
RUN apk add --no-cache --virtual .build-deps build-base linux-headers
RUN python3 -V
WORKDIR /opt/wallpaper

RUN pip install pipenv

COPY . ./

RUN pipenv sync


ENTRYPOINT ["pipenv", "run", "python3", "manage.py"]

CMD ["runserver"]
